package com.ruoyi.cms.controller;

import com.ruoyi.cms.domain.SysSite;
import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.cms.domain.CmsMaterialGroup;
import com.ruoyi.cms.service.ICmsMaterialGroupService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.core.domain.Ztree;

/**
 * 素材分组Controller
 * 
 * @author ruoyi
 * @date 2020-09-10
 */
@Controller
@RequestMapping("/cms/materialGroup")
public class CmsMaterialGroupController extends BaseController
{
    private String prefix = "cms/materialGroup";

    @Autowired
    private ICmsMaterialGroupService cmsMaterialGroupService;

    @RequiresPermissions("cms:materialGroup:view")
    @GetMapping()
    public String materialGroup()
    {
        return prefix + "/materialGroup";
    }

    /**
     * 查询素材分组树列表
     */
    @RequiresPermissions("cms:materialGroup:list")
    @PostMapping("/list")
    @ResponseBody
    public List<CmsMaterialGroup> list(CmsMaterialGroup cmsMaterialGroup)
    {
        SysSite sysSite = (SysSite) getSession().getAttribute("site");
        if (sysSite.getId() != 1) {
            if (cmsMaterialGroup.getSiteId() == null) {
                cmsMaterialGroup.setSiteId(sysSite.getId());
            }
            if (sysSite.getId() != cmsMaterialGroup.getSiteId()) {
                return null;
            }
        }
        List<CmsMaterialGroup> list = cmsMaterialGroupService.selectCmsMaterialGroupList(cmsMaterialGroup);
        return list;
    }

    /**
     * 导出素材分组列表
     */
    @RequiresPermissions("cms:materialGroup:export")
    @Log(title = "素材分组", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CmsMaterialGroup cmsMaterialGroup)
    {
        SysSite sysSite = (SysSite) getSession().getAttribute("site");
        if (sysSite.getId() != 1) {
            if (cmsMaterialGroup.getSiteId() == null) {
                cmsMaterialGroup.setSiteId(sysSite.getId());
            }
            if (sysSite.getId() != cmsMaterialGroup.getSiteId()) {
                return AjaxResult.error("站点权限不够");
            }
        }
        List<CmsMaterialGroup> list = cmsMaterialGroupService.selectCmsMaterialGroupList(cmsMaterialGroup);
        ExcelUtil<CmsMaterialGroup> util = new ExcelUtil<CmsMaterialGroup>(CmsMaterialGroup.class);
        return util.exportExcel(list, "materialGroup");
    }

    /**
     * 新增素材分组
     */
    @GetMapping(value = { "/add/{id}", "/add/" })
    public String add(@PathVariable(value = "id", required = false) Long id, ModelMap mmap)
    {
        SysSite sysSite = (SysSite) getSession().getAttribute("site");
        if (StringUtils.isNotNull(id))
        {
            mmap.put("cmsMaterialGroup", cmsMaterialGroupService.selectCmsMaterialGroupById(id, sysSite.getId()));
        }
        return prefix + "/add";
    }

    /**
     * 新增保存素材分组
     */
    @RequiresPermissions("cms:materialGroup:add")
    @Log(title = "素材分组", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CmsMaterialGroup cmsMaterialGroup)
    {   SysSite sysSite = (SysSite) getSession().getAttribute("site");
        if (sysSite.getId() != 1) {
            if (cmsMaterialGroup.getSiteId() == null) {
                cmsMaterialGroup.setSiteId(sysSite.getId());
            }
            if (sysSite.getId() != cmsMaterialGroup.getSiteId()) {
                return AjaxResult.error("站点权限不够");
            }
        }
        return toAjax(cmsMaterialGroupService.insertCmsMaterialGroup(cmsMaterialGroup));
    }

    /**
     * 修改素材分组
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        SysSite sysSite = (SysSite) getSession().getAttribute("site");
        CmsMaterialGroup cmsMaterialGroup = cmsMaterialGroupService.selectCmsMaterialGroupById(id, sysSite.getId());
        mmap.put("cmsMaterialGroup", cmsMaterialGroup);
        return prefix + "/edit";
    }

    /**
     * 修改保存素材分组
     */
    @RequiresPermissions("cms:materialGroup:edit")
    @Log(title = "素材分组", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CmsMaterialGroup cmsMaterialGroup)
    {
        SysSite sysSite = (SysSite) getSession().getAttribute("site");
        if (sysSite.getId() != 1) {
            if (cmsMaterialGroup.getSiteId() == null) {
                cmsMaterialGroup.setSiteId(sysSite.getId());
            }
            if (sysSite.getId() != cmsMaterialGroup.getSiteId()) {
                return AjaxResult.error("站点权限不够");
            }
        }
        return toAjax(cmsMaterialGroupService.updateCmsMaterialGroup(cmsMaterialGroup));
    }

    /**
     * 删除
     */
    @RequiresPermissions("cms:materialGroup:remove")
    @Log(title = "素材分组", businessType = BusinessType.DELETE)
    @GetMapping("/remove/{id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("id") Long id)
    {
        SysSite sysSite = (SysSite) getSession().getAttribute("site");

        return toAjax(cmsMaterialGroupService.deleteCmsMaterialGroupById(id, sysSite.getId()));
    }

    /**
     * 选择素材分组树
     */
    @GetMapping(value = { "/selectMaterialGroupTree/{id}", "/selectMaterialGroupTree/" })
    public String selectMaterialGroupTree(@PathVariable(value = "id", required = false) Long id, ModelMap mmap)
    {
        SysSite sysSite = (SysSite) getSession().getAttribute("site");
        if (StringUtils.isNotNull(id))
        {
            mmap.put("cmsMaterialGroup", cmsMaterialGroupService.selectCmsMaterialGroupById(id, sysSite.getId()));
        }
        return prefix + "/tree";
    }

    /**
     * 加载素材分组树列表
     */
    @GetMapping("/treeData")
    @ResponseBody
    public List<Ztree> treeData()
    {
        SysSite sysSite = (SysSite) getSession().getAttribute("site");
        List<Ztree> ztrees = cmsMaterialGroupService.selectCmsMaterialGroupTree(sysSite.getId());
        return ztrees;
    }
}
