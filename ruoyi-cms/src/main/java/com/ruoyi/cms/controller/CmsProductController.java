package com.ruoyi.cms.controller;

import com.ruoyi.cms.domain.SysSite;
import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.cms.domain.CmsProduct;
import com.ruoyi.cms.service.ICmsProductService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.cms.domain.CmsMaterial;

/**
 * 产品Controller
 * 
 * @author ruoyi
 * @date 2020-09-12
 */
@Controller
@RequestMapping("/cms/product")
public class CmsProductController extends BaseController
{
    private String prefix = "cms/product";

    @Autowired
    private ICmsProductService cmsProductService;

    @RequiresPermissions("cms:product:view")
    @GetMapping()
    public String product()
    {
        return prefix + "/product";
    }

    /**
     * 查询产品列表
     */
    @RequiresPermissions("cms:product:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(CmsProduct cmsProduct)
    {
        startPage();
        SysSite sysSite = (SysSite) getSession().getAttribute("site");
        if (sysSite.getId() != 1) {
            if (cmsProduct.getSiteId() == null) {
                cmsProduct.setSiteId(sysSite.getId());
            }
            if (sysSite.getId() != cmsProduct.getSiteId()) {
                return errorDataTable("站点权限不够");
            }
        }
        List<CmsProduct> list = cmsProductService.selectCmsProductList(cmsProduct);
        return getDataTable(list);
    }

    /**
     * 导出产品列表
     */
    @RequiresPermissions("cms:product:export")
    @Log(title = "产品", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CmsProduct cmsProduct)
    {
        SysSite sysSite = (SysSite) getSession().getAttribute("site");
        if (sysSite.getId() != 1) {
            if (cmsProduct.getSiteId() == null) {
                cmsProduct.setSiteId(sysSite.getId());
            }
            if (sysSite.getId() != cmsProduct.getSiteId()) {
                return AjaxResult.error("站点权限不够");
            }
        }
        List<CmsProduct> list = cmsProductService.selectCmsProductList(cmsProduct);
        ExcelUtil<CmsProduct> util = new ExcelUtil<CmsProduct>(CmsProduct.class);
        return util.exportExcel(list, "product");
    }

    /**
     * 新增产品
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存产品
     */
    @RequiresPermissions("cms:product:add")
    @Log(title = "产品", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CmsProduct cmsProduct)
    {   SysSite sysSite = (SysSite) getSession().getAttribute("site");
        if (sysSite.getId() != 1) {
            if (cmsProduct.getSiteId() == null) {
                cmsProduct.setSiteId(sysSite.getId());
            }
            if (sysSite.getId() != cmsProduct.getSiteId()) {
                return AjaxResult.error("站点权限不够");
            }
        }
        return toAjax(cmsProductService.insertCmsProduct(cmsProduct));
    }

    /**
     * 修改产品
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        SysSite sysSite = (SysSite) getSession().getAttribute("site");
        CmsProduct cmsProduct = cmsProductService.selectCmsProductById(id, sysSite.getId());
        mmap.put("cmsProduct", cmsProduct);
        return prefix + "/edit";
    }

    /**
     * 修改保存产品
     */
    @RequiresPermissions("cms:product:edit")
    @Log(title = "产品", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CmsProduct cmsProduct)
    {
        SysSite sysSite = (SysSite) getSession().getAttribute("site");
        if (sysSite.getId() != 1) {
            if (cmsProduct.getSiteId() == null) {
                cmsProduct.setSiteId(sysSite.getId());
            }
            if (sysSite.getId() != cmsProduct.getSiteId()) {
                return AjaxResult.error("站点权限不够");
            }
        }
        return toAjax(cmsProductService.updateCmsProduct(cmsProduct));
    }

    /**
     * 删除产品
     */
    @RequiresPermissions("cms:product:remove")
    @Log(title = "产品", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        SysSite sysSite = (SysSite) getSession().getAttribute("site");
        return toAjax(cmsProductService.deleteCmsProductByIds(ids, sysSite.getId()));
    }

    /**
     * 跳转分配素材页面
     */
    @RequiresPermissions("wjgl:product:addMaterial")
    @GetMapping("/toAddMaterial/{id}")
    public String toAddMaterial(@PathVariable("id") Long id, ModelMap mmap)
    {
        mmap.put("id", id);
        return prefix + "/addMaterial";
    }

    /**
     * 查询产品绑定的素材图片列表
     *
     */
    @PostMapping("/materialList")
    @ResponseBody
    public TableDataInfo materialList(CmsProduct cmsProduct)
    {
        SysSite sysSite = (SysSite) getSession().getAttribute("site");
        List<CmsProduct> list = cmsProductService.selectMaterialList(cmsProduct, sysSite.getId());
        return getDataTable(list);
    }

    /**
     * 跳转选择未分配素材页面
     */
    @GetMapping("/selectMaterial/{id}")
    public String selectMaterial(@PathVariable("id") Integer id, ModelMap mmap)
    {
        mmap.put("id", id);
        return prefix + "/selectMaterial";
    }

    /**
     * 查询未配置的素材
     * @param cmsProduct
     * @return
     */
    @PostMapping("/unMaterialList")
    @ResponseBody
    public TableDataInfo unMaterialList(CmsProduct cmsProduct)
    {
        startPage();
        SysSite sysSite = (SysSite) getSession().getAttribute("site");
        List<CmsMaterial> list = cmsProductService.selectUnMaterialList(cmsProduct, sysSite.getId());
        return getDataTable(list);
    }

    /**
     * 保存素材
     */
    @PostMapping( "/saveMaterial")
    @ResponseBody
    public AjaxResult saveMaterial(String id,String materialIds)
    {
        if(StringUtils.isEmpty(id)||StringUtils.isEmpty(materialIds)){
            return AjaxResult.error("参数传递错误!");
        }
        SysSite sysSite = (SysSite) getSession().getAttribute("site");
        cmsProductService.saveMaterial(id, materialIds, sysSite.getId());
        return success();
    }

    /**
     * 删除关联的素材
     */
    @PostMapping( "/deleteMaterialBatch")
    @ResponseBody
    public AjaxResult deleteMaterialBatch(String ids, Long id)
    {
        if(StringUtils.isEmpty(ids)){
            return AjaxResult.error("参数传递错误!");
        }
        SysSite sysSite = (SysSite) getSession().getAttribute("site");
        cmsProductService.deleteMaterialBatch(ids, id, sysSite.getId());
        return success();
    }

}
