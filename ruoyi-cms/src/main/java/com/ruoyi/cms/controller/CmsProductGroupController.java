package com.ruoyi.cms.controller;

import com.ruoyi.cms.domain.SysSite;
import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.cms.domain.CmsProductGroup;
import com.ruoyi.cms.service.ICmsProductGroupService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.core.domain.Ztree;

/**
 * 产品分组Controller
 * 
 * @author ruoyi
 * @date 2020-09-12
 */
@Controller
@RequestMapping("/cms/productGroup")
public class CmsProductGroupController extends BaseController
{
    private String prefix = "cms/productGroup";

    @Autowired
    private ICmsProductGroupService cmsProductGroupService;

    @RequiresPermissions("cms:productGroup:view")
    @GetMapping()
    public String productGroup()
    {
        return prefix + "/productGroup";
    }

    /**
     * 查询产品分组树列表
     */
    @RequiresPermissions("cms:productGroup:list")
    @PostMapping("/list")
    @ResponseBody
    public List<CmsProductGroup> list(CmsProductGroup cmsProductGroup)
    {
        SysSite sysSite = (SysSite) getSession().getAttribute("site");
        if (sysSite.getId() != 1) {
            if (cmsProductGroup.getSiteId() == null) {
                cmsProductGroup.setSiteId(sysSite.getId());
            }
            if (sysSite.getId() != cmsProductGroup.getSiteId()) {
                return null;
            }
        }
        List<CmsProductGroup> list = cmsProductGroupService.selectCmsProductGroupList(cmsProductGroup);
        return list;
    }

    /**
     * 导出产品分组列表
     */
    @RequiresPermissions("cms:productGroup:export")
    @Log(title = "产品分组", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(CmsProductGroup cmsProductGroup)
    {
        SysSite sysSite = (SysSite) getSession().getAttribute("site");
        if (sysSite.getId() != 1) {
            if (cmsProductGroup.getSiteId() == null) {
                cmsProductGroup.setSiteId(sysSite.getId());
            }
            if (sysSite.getId() != cmsProductGroup.getSiteId()) {
                return AjaxResult.error("站点权限不够");
            }
        }
        List<CmsProductGroup> list = cmsProductGroupService.selectCmsProductGroupList(cmsProductGroup);
        ExcelUtil<CmsProductGroup> util = new ExcelUtil<CmsProductGroup>(CmsProductGroup.class);
        return util.exportExcel(list, "productGroup");
    }

    /**
     * 新增产品分组
     */
    @GetMapping(value = { "/add/{id}", "/add/" })
    public String add(@PathVariable(value = "id", required = false) Long id, ModelMap mmap)
    {
        SysSite sysSite = (SysSite) getSession().getAttribute("site");
        if (StringUtils.isNotNull(id))
        {
            mmap.put("cmsProductGroup", cmsProductGroupService.selectCmsProductGroupById(id, sysSite.getId()));
        }
        return prefix + "/add";
    }

    /**
     * 新增保存产品分组
     */
    @RequiresPermissions("cms:productGroup:add")
    @Log(title = "产品分组", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(CmsProductGroup cmsProductGroup)
    {   SysSite sysSite = (SysSite) getSession().getAttribute("site");
        if (sysSite.getId() != 1) {
            if (cmsProductGroup.getSiteId() == null) {
                cmsProductGroup.setSiteId(sysSite.getId());
            }
            if (sysSite.getId() != cmsProductGroup.getSiteId()) {
                return AjaxResult.error("站点权限不够");
            }
        }
        return toAjax(cmsProductGroupService.insertCmsProductGroup(cmsProductGroup));
    }

    /**
     * 修改产品分组
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        SysSite sysSite = (SysSite) getSession().getAttribute("site");
        CmsProductGroup cmsProductGroup = cmsProductGroupService.selectCmsProductGroupById(id, sysSite.getId());
        mmap.put("cmsProductGroup", cmsProductGroup);
        return prefix + "/edit";
    }

    /**
     * 修改保存产品分组
     */
    @RequiresPermissions("cms:productGroup:edit")
    @Log(title = "产品分组", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(CmsProductGroup cmsProductGroup)
    {
        SysSite sysSite = (SysSite) getSession().getAttribute("site");
        if (sysSite.getId() != 1) {
            if (cmsProductGroup.getSiteId() == null) {
                cmsProductGroup.setSiteId(sysSite.getId());
            }
            if (sysSite.getId() != cmsProductGroup.getSiteId()) {
                return AjaxResult.error("站点权限不够");
            }
        }
        return toAjax(cmsProductGroupService.updateCmsProductGroup(cmsProductGroup));
    }

    /**
     * 删除
     */
    @RequiresPermissions("cms:productGroup:remove")
    @Log(title = "产品分组", businessType = BusinessType.DELETE)
    @GetMapping("/remove/{id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("id") Long id)
    {
        SysSite sysSite = (SysSite) getSession().getAttribute("site");

        return toAjax(cmsProductGroupService.deleteCmsProductGroupById(id, sysSite.getId()));
    }

    /**
     * 选择产品分组树
     */
    @GetMapping(value = { "/selectProductGroupTree/{id}", "/selectProductGroupTree/" })
    public String selectProductGroupTree(@PathVariable(value = "id", required = false) Long id, ModelMap mmap)
    {
        SysSite sysSite = (SysSite) getSession().getAttribute("site");
        if (StringUtils.isNotNull(id))
        {
            mmap.put("cmsProductGroup", cmsProductGroupService.selectCmsProductGroupById(id, sysSite.getId()));
        }
        return prefix + "/tree";
    }

    /**
     * 加载产品分组树列表
     */
    @GetMapping("/treeData")
    @ResponseBody
    public List<Ztree> treeData()
    {
        SysSite sysSite = (SysSite) getSession().getAttribute("site");
        List<Ztree> ztrees = cmsProductGroupService.selectCmsProductGroupTree(sysSite.getId());
        return ztrees;
    }

}
