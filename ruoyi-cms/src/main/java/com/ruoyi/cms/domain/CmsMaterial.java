package com.ruoyi.cms.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 素材对象 cms_material
 * 
 * @author ruoyi
 * @date 2020-09-10
 */
public class CmsMaterial extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 分组id */
    @Excel(name = "分组id")
    private Long groupId;

    /** 站点id */
    @Excel(name = "站点id")
    private Long siteId;

    /** 素材名称 */
    @Excel(name = "素材名称")
    private String materialName;

    /** 素材类型 字典 1图片2视频 */
    @Excel(name = "素材类型 字典 1图片2视频")
    private Integer materialType;

    /** 素材描述 */
    @Excel(name = "素材描述")
    private String description;

    /** 素材大小 */
    @Excel(name = "素材大小")
    private String materialSize;

    /** 保存路径 */
    @Excel(name = "保存路径")
    private String savePath;

    /** 分辨率 宽 */
    @Excel(name = "分辨率 宽")
    private String width;

    /** 分辨率 高 */
    @Excel(name = "分辨率 高")
    private String height;

    /** 排序 */
    @Excel(name = "排序")
    private Long sort;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setGroupId(Long groupId) 
    {
        this.groupId = groupId;
    }

    public Long getGroupId() 
    {
        return groupId;
    }
    public void setSiteId(Long siteId) 
    {
        this.siteId = siteId;
    }

    public Long getSiteId() 
    {
        return siteId;
    }
    public void setMaterialName(String materialName) 
    {
        this.materialName = materialName;
    }

    public String getMaterialName() 
    {
        return materialName;
    }
    public void setMaterialType(Integer materialType) 
    {
        this.materialType = materialType;
    }

    public Integer getMaterialType() 
    {
        return materialType;
    }
    public void setDescription(String description) 
    {
        this.description = description;
    }

    public String getDescription() 
    {
        return description;
    }
    public void setMaterialSize(String materialSize) 
    {
        this.materialSize = materialSize;
    }

    public String getMaterialSize() 
    {
        return materialSize;
    }
    public void setSavePath(String savePath) 
    {
        this.savePath = savePath;
    }

    public String getSavePath() 
    {
        return savePath;
    }
    public void setWidth(String width) 
    {
        this.width = width;
    }

    public String getWidth() 
    {
        return width;
    }
    public void setHeight(String height) 
    {
        this.height = height;
    }

    public String getHeight() 
    {
        return height;
    }
    public void setSort(Long sort) 
    {
        this.sort = sort;
    }

    public Long getSort() 
    {
        return sort;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("groupId", getGroupId())
            .append("siteId", getSiteId())
            .append("materialName", getMaterialName())
            .append("materialType", getMaterialType())
            .append("description", getDescription())
            .append("materialSize", getMaterialSize())
            .append("savePath", getSavePath())
            .append("width", getWidth())
            .append("height", getHeight())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .append("sort", getSort())
            .toString();
    }
}
