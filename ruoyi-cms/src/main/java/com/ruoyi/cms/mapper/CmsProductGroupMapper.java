package com.ruoyi.cms.mapper;

import java.util.List;
import com.ruoyi.cms.domain.CmsProductGroup;
import org.apache.ibatis.annotations.Param;

/**
 * 产品分组Mapper接口
 *
 * @author ruoyi
 * @date 2020-09-12
 */
public interface CmsProductGroupMapper
{
    /**
     * 查询产品分组
     *
     * @param id 产品分组ID
     * @return 产品分组
     */
    public CmsProductGroup selectCmsProductGroupById(@Param("id") Long id, @Param("siteId") Long siteId);

    /**
     * 查询产品分组列表
     *
     * @param cmsProductGroup 产品分组
     * @return 产品分组集合
     */
    public List<CmsProductGroup> selectCmsProductGroupList(CmsProductGroup cmsProductGroup);

    /**
     * 新增产品分组
     *
     * @param cmsProductGroup 产品分组
     * @return 结果
     */
    public int insertCmsProductGroup(CmsProductGroup cmsProductGroup);

    /**
     * 修改产品分组
     *
     * @param cmsProductGroup 产品分组
     * @return 结果
     */
    public int updateCmsProductGroup(CmsProductGroup cmsProductGroup);

    /**
     * 删除产品分组
     *
     * @param id 产品分组ID
     * @return 结果
     */
    public int deleteCmsProductGroupById(@Param("id") Long id, @Param("siteId") Long siteId);

    /**
     * 批量删除产品分组
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCmsProductGroupByIds(@Param("ids") String[] ids,  @Param("siteId") Long siteId);
}
