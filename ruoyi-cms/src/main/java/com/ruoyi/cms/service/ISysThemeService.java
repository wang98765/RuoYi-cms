package com.ruoyi.cms.service;

import java.util.List;
import com.ruoyi.cms.domain.SysTheme;
import com.ruoyi.common.core.domain.Ztree;

/**
 * 模板主题Service接口
 * 
 * @author ruoyi
 * @date 2020-09-12
 */
public interface ISysThemeService 
{
    /**
     * 查询模板主题
     * 
     * @param id 模板主题ID
     * @return 模板主题
     */
    public SysTheme selectSysThemeById(Long id);

    /**
     * 查询模板主题列表
     * 
     * @param sysTheme 模板主题
     * @return 模板主题集合
     */
    public List<SysTheme> selectSysThemeList(SysTheme sysTheme);

    /**
     * 新增模板主题
     * 
     * @param sysTheme 模板主题
     * @return 结果
     */
    public int insertSysTheme(SysTheme sysTheme);

    /**
     * 修改模板主题
     * 
     * @param sysTheme 模板主题
     * @return 结果
     */
    public int updateSysTheme(SysTheme sysTheme);

    /**
     * 批量删除模板主题
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysThemeByIds(String ids);

    /**
     * 删除模板主题信息
     * 
     * @param id 模板主题ID
     * @return 结果
     */
    public int deleteSysThemeById(Long id);

    List<Ztree> selectTree();
}
