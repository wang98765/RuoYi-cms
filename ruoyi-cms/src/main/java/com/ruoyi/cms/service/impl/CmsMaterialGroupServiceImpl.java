package com.ruoyi.cms.service.impl;

import java.util.List;
import java.util.ArrayList;
import com.ruoyi.common.core.domain.Ztree;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.cms.mapper.CmsMaterialGroupMapper;
import com.ruoyi.cms.domain.CmsMaterialGroup;
import com.ruoyi.cms.service.ICmsMaterialGroupService;
import com.ruoyi.common.core.text.Convert;

/**
 * 素材分组Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-09-10
 */
@Service
public class CmsMaterialGroupServiceImpl implements ICmsMaterialGroupService 
{
    @Autowired
    private CmsMaterialGroupMapper cmsMaterialGroupMapper;

    /**
     * 查询素材分组
     * 
     * @param id 素材分组ID
     * @return 素材分组
     */
    @Override
    public CmsMaterialGroup selectCmsMaterialGroupById(Long id, Long siteId)
    {
        return cmsMaterialGroupMapper.selectCmsMaterialGroupById(id, siteId);
    }

    /**
     * 查询素材分组列表
     * 
     * @param cmsMaterialGroup 素材分组
     * @return 素材分组
     */
    @Override
    public List<CmsMaterialGroup> selectCmsMaterialGroupList(CmsMaterialGroup cmsMaterialGroup)
    {
        return cmsMaterialGroupMapper.selectCmsMaterialGroupList(cmsMaterialGroup);
    }

    /**
     * 新增素材分组
     * 
     * @param cmsMaterialGroup 素材分组
     * @return 结果
     */
    @Override
    public int insertCmsMaterialGroup(CmsMaterialGroup cmsMaterialGroup)
    {
        cmsMaterialGroup.setCreateTime(DateUtils.getNowDate());
        return cmsMaterialGroupMapper.insertCmsMaterialGroup(cmsMaterialGroup);
    }

    /**
     * 修改素材分组
     * 
     * @param cmsMaterialGroup 素材分组
     * @return 结果
     */
    @Override
    public int updateCmsMaterialGroup(CmsMaterialGroup cmsMaterialGroup)
    {
        cmsMaterialGroup.setUpdateTime(DateUtils.getNowDate());
        return cmsMaterialGroupMapper.updateCmsMaterialGroup(cmsMaterialGroup);
    }

    /**
     * 删除素材分组对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteCmsMaterialGroupByIds(String ids, Long siteId)
    {
        return cmsMaterialGroupMapper.deleteCmsMaterialGroupByIds(Convert.toStrArray(ids), siteId);
    }

    /**
     * 删除素材分组信息
     * 
     * @param id 素材分组ID
     * @return 结果
     */
    @Override
    public int deleteCmsMaterialGroupById(Long id, Long siteId)
    {
        return cmsMaterialGroupMapper.deleteCmsMaterialGroupById(id, siteId);
    }

    /**
     * 查询素材分组树列表
     * 
     * @return 所有素材分组信息
     */
    @Override
    public List<Ztree> selectCmsMaterialGroupTree(Long siteId)
    {
        CmsMaterialGroup cmsMaterialGroupP = new CmsMaterialGroup();
        if (siteId == 1) {
                cmsMaterialGroupP.setSiteId(null);
        } else {
                cmsMaterialGroupP.setSiteId(siteId);
        }

        List<CmsMaterialGroup> cmsMaterialGroupList = cmsMaterialGroupMapper.selectCmsMaterialGroupList(cmsMaterialGroupP);
        List<Ztree> ztrees = new ArrayList<Ztree>();
        for (CmsMaterialGroup cmsMaterialGroup : cmsMaterialGroupList)
        {
            Ztree ztree = new Ztree();
            ztree.setId(cmsMaterialGroup.getId());
            ztree.setpId(cmsMaterialGroup.getParentId());
            ztree.setName(cmsMaterialGroup.getGroupName());
            ztree.setTitle(cmsMaterialGroup.getGroupName());
            ztrees.add(ztree);
        }
        return ztrees;
    }
}
