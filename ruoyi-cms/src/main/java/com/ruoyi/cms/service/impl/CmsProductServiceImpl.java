package com.ruoyi.cms.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.cms.domain.CmsMaterial;
import com.ruoyi.cms.mapper.CmsProductMapper;
import com.ruoyi.cms.domain.CmsProduct;
import com.ruoyi.cms.service.ICmsProductService;
import com.ruoyi.common.core.text.Convert;

/**
 * 产品Service业务层处理
 *
 * @author ruoyi
 * @date 2020-09-12
 */
@Service
public class CmsProductServiceImpl implements ICmsProductService
{
    @Autowired
    private CmsProductMapper cmsProductMapper;

    /**
     * 查询产品
     *
     * @param id 产品ID
     * @return 产品
     */
    @Override
    public CmsProduct selectCmsProductById(Long id, Long siteId)
    {
        return cmsProductMapper.selectCmsProductById(id, siteId);
    }

    /**
     * 查询产品列表
     *
     * @param cmsProduct 产品
     * @return 产品
     */
    @Override
    public List<CmsProduct> selectCmsProductList(CmsProduct cmsProduct)
    {
        return cmsProductMapper.selectCmsProductList(cmsProduct);
    }

    /**
     * 新增产品
     *
     * @param cmsProduct 产品
     * @return 结果
     */
    @Override
    public int insertCmsProduct(CmsProduct cmsProduct)
    {
        cmsProduct.setCreateTime(DateUtils.getNowDate());
        return cmsProductMapper.insertCmsProduct(cmsProduct);
    }

    /**
     * 修改产品
     *
     * @param cmsProduct 产品
     * @return 结果
     */
    @Override
    public int updateCmsProduct(CmsProduct cmsProduct)
    {
        cmsProduct.setUpdateTime(DateUtils.getNowDate());
        return cmsProductMapper.updateCmsProduct(cmsProduct);
    }

    /**
     * 删除产品对象
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteCmsProductByIds(String ids, Long siteId)
    {
        return cmsProductMapper.deleteCmsProductByIds(Convert.toStrArray(ids), siteId);
    }

    /**
     * 删除产品信息
     *
     * @param id 产品ID
     * @return 结果
     */
    @Override
    public int deleteCmsProductById(Long id, Long siteId)
    {
        return cmsProductMapper.deleteCmsProductById(id, siteId);
    }

    @Override
    public List<CmsProduct> selectMaterialList(CmsProduct cmsProduct, Long siteId) {
        cmsProduct.setSiteId(siteId);
        return cmsProductMapper.selectMaterialList(cmsProduct);
    }

    @Override
    public List<CmsMaterial> selectUnMaterialList(CmsProduct cmsProduct, Long siteId) {
        cmsProduct.setSiteId(siteId);
        return cmsProductMapper.selectUnMaterialList(cmsProduct);
    }

    @Override
    public void saveMaterial(String id, String materialIds, Long siteId) {
        String[] arr = Convert.toStrArray(materialIds);
        cmsProductMapper.saveMaterial(id, arr, siteId);
    }

    @Override
    public void deleteMaterialBatch(String materialIds, Long id, Long siteId) {
        String[] arr = Convert.toStrArray(materialIds);
        cmsProductMapper.deleteMaterialBatch(arr, id, siteId);
    }
}
