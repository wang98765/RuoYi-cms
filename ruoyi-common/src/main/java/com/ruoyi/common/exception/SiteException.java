package com.ruoyi.common.exception;

import com.ruoyi.common.exception.user.UserException;

/**
 * 站点异常类
 * 
 * @author ruoyi
 */
public class SiteException extends UserException
{
    private static final long serialVersionUID = 1L;

    public SiteException()
    {
        super("siteException", null);
    }
}
